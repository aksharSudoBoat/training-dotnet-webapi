using AuditBook;
using AuditBook.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AuditBook.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class AuditBookController : ControllerBase
    {
        private readonly AuditBookDbContext dbContext;

        public AuditBookController(AuditBookDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetAuditBook()
        {
            return Ok(await dbContext.AuditBook.ToListAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddAuditEntry(AuditEntry auditEntry)
        {
            var entry = new AuditEntry()
            {
                id = Guid.NewGuid(),
                category = auditEntry.category,
                invoiceToFrom = auditEntry.invoiceToFrom,
                serviceDescription = auditEntry.serviceDescription,
                invoiceNo = auditEntry.invoiceNo,
                amount = auditEntry.amount,
                date = auditEntry.date,
            };

            await dbContext.AuditBook.AddAsync(entry);
            await dbContext.SaveChangesAsync();

            Console.WriteLine(entry);
            return Ok(entry);
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetAuditEntry([FromRoute] Guid id)
        {
            var entry = await dbContext.AuditBook.FindAsync(id);
            if (entry == null)
            {
                return NotFound("Data doesn't exists!");
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task<IActionResult> DeleteAuditEntry([FromRoute] Guid id)
        {
            var entry = await dbContext.AuditBook.FindAsync(id);
            if (entry != null)
            {
                dbContext.Remove(entry);
                await dbContext.SaveChangesAsync();
                return Ok("Removed Successfully!");
            }
            return NotFound("Data doesn't exists!");

        }

        [HttpPut]
        [Route("{id:guid}")]
        public async Task<IActionResult> UpdateAuditEntry([FromRoute] Guid id, AuditEntry updateAuditEntry)

        {
            var entry = await dbContext.AuditBook.FindAsync(id);

            if (entry != null)
            {
                entry.category = updateAuditEntry.category;
                entry.invoiceToFrom = updateAuditEntry.invoiceToFrom;
                entry.serviceDescription = updateAuditEntry.serviceDescription;
                entry.invoiceNo = updateAuditEntry.invoiceNo;
                entry.amount = updateAuditEntry.amount;
                entry.date = updateAuditEntry.date;

                await dbContext.SaveChangesAsync();

                return Ok(entry);
            }

            return NotFound();
        }
    }
}
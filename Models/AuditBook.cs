using System.ComponentModel.DataAnnotations;
namespace AuditBook.Models{

 public class AuditEntry{
    [Key]
    public Guid id {get; set;}
    public string category {get; set;}

    public string invoiceToFrom {get; set;}

    public string serviceDescription {get; set;}

    public string invoiceNo {get; set;}

    public int amount {get; set;}

    public DateTime date {get; set;}
 }
}


using Microsoft.EntityFrameworkCore;
using AuditBook.Models;
namespace AuditBook{

    public class AuditBookDbContext: DbContext {
        // internal readonly object AuditEntry;

        public AuditBookDbContext(DbContextOptions options): base(options) {

}

public  DbSet<AuditEntry>AuditBook { get; set; }
    }
}